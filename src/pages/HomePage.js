import React from 'react';
import Header from '../components/Header';
import Description from '../components/Description';
import SelfIntroduction from '../components/SelfIntroduction';
import Linkers from '../components/Linkers';
import Slider from '../components/Slider';
import ContactPage from './ContactPage';


class HomePage extends React.Component {
    render() {
        return(
            <div>
                <Header />
                <Description />
                <SelfIntroduction />
                <Linkers />
                <Slider />
                <ContactPage />
            </div>
        );
    }
}

export default HomePage;