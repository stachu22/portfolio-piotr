import React from 'react';
import { Link } from 'react-router-dom';
import Articles from '../components/Articles';
import { ArticlesFilterContainer } from '../components/ArticlesFilter';
import { connect } from 'react-redux';
import { articlesFetched } from '../actions';
import { getFilteredArticles } from '../selectors/getFilteredArticles';


class Blog extends React.Component {
    componentDidMount() {
        fetch('http://localhost:8000/blog/articles/')
            .then(response => response.json())
            .then(json => this.props.articlesFetched(json))
    }
    render() {
        return(
            <section className="section4">
                <div className="container">
                    <div>
                        <h1>Blog <Link to="/login"><button type="button" className="btn btn-default navbar-btn">Sign in</button></Link><ArticlesFilterContainer /></h1>  
                    </div>
                <hr/>
                <br/>
                    <div className="row justify-content-center">
                        <Articles articles={ this.props.articles }/>
                    </div>
                </div>  
                <nav>
                    <ul className="pagination d-flex justify-content-center">
                        <li className="page-item disabled">
                            <a className="page-link" href="/" tabIndex="-1">Previous</a>
                        </li>
                        <li className="page-item"><a className="page-link" href="/">1</a></li>
                        <li className="page-item"><a className="page-link" href="/">2</a></li>
                        <li className="page-item">
                            <a className="page-link" href="/">Next</a>
                        </li>
                    </ul>
                </nav>                        
            </section>         
           
        );
    }
}

const mapStateToProps = (state) => {
    return {
        articles: getFilteredArticles(state.articles, state.articlesSearch)
    };
};

const mapDispatchToProps = { articlesFetched };

export const BlogContainer = connect(
    mapStateToProps, 
    mapDispatchToProps
)(Blog);