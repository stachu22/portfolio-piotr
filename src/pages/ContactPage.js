import React from 'react';
import Contact from '../components/Contact';


const newEmail = {
    fullname: '',
    email: '',
    message: '',
};

const ContactPage = () => (
    <Contact mail = { newEmail }/>
);

export default ContactPage;