import React from 'react';
import SignInForm from '../components/SignInForm';


const signInUser = {
    email: "",
    password: "",
};

const SignInPage = () => (
    <section className="background-photo">
        <div className="container">
            <div className="row">
                <div className="col-md-7 col-sm-0">
                   
                </div>
                <div className="col-md-5 col-sm-12">
                    <div className="card login-form-margin">
                        <SignInForm user = { signInUser } />
                    </div>    
                </div>
            </div>
        </div>
    </section>
);

export default SignInPage;