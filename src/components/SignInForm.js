import React from 'react';
import { withFormik, Form, Field } from 'formik';
import * as Yup from 'yup';


export const SignIn = (props) => {
    const {
        touched,
        errors,
    } = props;

    return(
        <Form>
                <h1>Sign in to blog</h1>
                <div>
                    <h4>Email:</h4>
                    <Field className="form-margin form-control rounded-0" name="email" type="email"  />
                    { touched.email && errors.email && <p className="errors">{ errors.email }</p> }
                </div>
                <div>
                    <h4>Password:</h4>    
                    <Field className="form-margin form-control rounded-0" name="password" type="password" />
                    { touched.password && errors.password && <p className="errors">{ errors.password }</p> }
                </div>    
                <button type="submit" className="btn btn-secondary btn-block rounded-0">Sign In</button>
            </Form>
    );
}

export default withFormik({
    mapPropsToValues: (props) => ({
        email: props.user.email,
        password: props.user.password,
    }),

    validationSchema: Yup.object().shape({
        email: Yup.string().email("Please enter a valid email adress").required("Email is a required field"),
        password: Yup.string().min(8, "Password must contain at least 8 characters").required("Password is a required field")
    }),

    handleSubmit(values) {
        alert('Email: ' + values.email + '\nPassword: ' + values.password);
    }
})(SignIn);



