import React from 'react';


class Slider extends React.Component {
    render() {
        return(
            <section>
                <div className="slider carousel-shadow">
                    <div id="carouselID" className="carousel slide" data-ride="carousel">
                        <div className="carousel-inner carousel-shadow">
                          <div className="item active shadow">
                            <img src={require('../MEDIA/p1.jpg')} alt="Pizza" />
                            <div className="carousel-caption d-none d-md-block">
                                <h3>Bla bla bla</h3>
                                <p>
                                    Ogólnie znana teza głosi, iż użytkownika może rozpraszać zrozumiała zawartość strony, kiedy ten chce zobaczyć sam jej wygląd. </p>
                            </div>
                          </div>
                          <div className="item">
                            <img src={require('../MEDIA/p2.jpg')} alt="Pizza" />
                            <div className="carousel-caption d-none d-md-block">
                                    <h3>Bla bla bla</h3>
                                    <p>
                                        Ogólnie znana teza głosi, iż użytkownika może rozpraszać zrozumiała zawartość strony, kiedy ten chce zobaczyć sam jej wygląd.  </p>
                            </div>
                          </div>
                          <div className="item shadow">
                            <img src={require('../MEDIA/p3.jpg')} alt="Pizza" />
                            <div className="carousel-caption d-none d-md-block">
                                    <h3>Bla bla bla</h3>
                                    <p>
                                        Ogólnie znana teza głosi, iż użytkownika może rozpraszać zrozumiała zawartość strony, kiedy ten chce zobaczyć sam jej wygląd.  </p>
                                </div>
                          </div>
                        </div>
                        <a className="carousel-control-prev" href="#carouselID" role="button" data-slide="prev">
                          <span className="carousel-control-prev-icon" aria-hidden="true"></span>
                        </a>
                        <a className="carousel-control-next" href="#carouselID" role="button" data-slide="next">
                          <span className="carousel-control-next-icon" aria-hidden="true"></span>
                        </a>
                        <ol className="carousel-indicators">
                          <li data-target="#carouselID" data-slide-to="0" className="active"></li>
                          <li data-target="#carouselID" data-slide-to="1"></li>
                          <li data-target="#carouselID" data-slide-to="2"></li>
                        </ol>
                    </div>   
                </div>                 
            </section>
        );
    }
}

export default Slider;