import React from 'react';


class Footer extends React.Component {
    render() {
        return(
            <section>
                <footer>
                    <div className="container">
                        <div className="row">
                            <div className="col-md-12 text-center social-icons">
                                <a href="https://www.facebook.com/"><i className="fa fa-facebook-square i-size-footer i"></i></a>
                                <a href="https://pl.linkedin.com/"><i className="fa fa-linkedin-square i-size-footer i"></i></a>
                                <a href="https://twitter.com/"><i className="fa fa-twitter i-size-footer i"></i></a>
                            </div>
                            <div className="text-center col-md-12">
                                <p>&copy;Stanisław Polichnowski</p>
                            </div>
                        </div>
                    </div>
                </footer>
            </section>
        );
    }
}

export default Footer;