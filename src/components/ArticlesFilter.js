import React from 'react';
import { connect } from 'react-redux';
import { searchArticles } from '../actions';


class ArticlesFilter extends React.Component {
    render() {
        return(
            <div className="col-md-11 input-group">
                <input 
                    className="form-control py-2 border-right-0 border"
                    type="search" 
                    placeholder="Search" 
                    value={ this.props.articlesSearch }
                    onChange={ this.handleSearchChange }
                />
                 <span className="input-group-append">
                  <div className="input-group-text search-style"><i className="fa fa-search search-size"></i></div>
            </span>
            </div>
        );
    };
    handleSearchChange = event => {
        this.props.searchArticles(event.currentTarget.value);
    };
};

const mapStateToProps = (state) => {
    return{
        articlesSearch: state.articlesSearch
    };
};

const mapDispatchToProps = { searchArticles };

export const ArticlesFilterContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(ArticlesFilter);