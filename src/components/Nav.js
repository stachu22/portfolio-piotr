import React from 'react';
import { Link } from 'react-router-dom';


class Nav extends React.Component {
    render() {
        return(
            <section>
                <nav className="navbar navbar-expand-md navbar-light bg-light rounded-0" data-spy="affix">
                    <a className="navbar-brand">Piotr Polichnowski</a>
                    <button type="button" className="navbar-toggle ml-auto" data-toggle="collapse" data-target="#collapse-1" aria-expanded="false">
                        <span className="navbar-toggler-icon"></span>
                    </button>
                    <div className="collapse navbar-collapse" id="collapse-1">
                        <ul className="navbar-nav">
                            <li className="nav-item"><Link className="nav-link" to="/Home">Home</Link></li>
                            <li className="nav-item"><Link className="nav-link" to="/Menu">Menu</Link></li>
                            <li className="nav-item"><Link className="nav-link" to="">CV</Link></li>
                            <li className="nav-item"><Link className="nav-link" to="/Portfolio">Portfolio</Link></li>
                            <li className="nav-item"><Link className="nav-link" to="/Blog">Blog</Link></li>
                            <li className="nav-item"><a className="nav-link" href="/Home#contact">Contact</a></li>
                        </ul>
                    </div>
                    <div className="nav ml-auto social-icons display">
                        <a href="https://www.facebook.com/"><i className="fa fa-facebook-square i-size-nav i"></i></a>
                        <a href="https://pl.linkedin.com/"><i className="fa fa-linkedin-square i-size-nav i"></i></a>
                        <a href="https://twitter.com/"><i className="fa fa-twitter i-size-nav i"></i></a>
                    </div>
                </nav>
            </section>
        );
    }
}

export default Nav;