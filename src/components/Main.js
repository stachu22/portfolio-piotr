import React from 'react';
import { Switch, Route } from 'react-router-dom';

import { BlogContainer } from '../pages/Blog';
import Menu from '../pages/Menu';
import Portfolio from '../pages/Portfolio';
import HomePage from '../pages/HomePage';
import SignInPage from '../pages/SignInPage';
// import Article from './Article';


class Main extends React.Component {
    render() {
        // const articlePath = `./Blog/${ this.props.id }`;
        return(
            <main>
                <Switch>
                    <Route path="/Home" component={ HomePage } />
                    <Route path="/Menu" component={ Menu } />
                    <Route path="/Blog" component={ BlogContainer } />
                    {/* <Route path="/Blog/1" component={ Article } /> */}
                    <Route path="/Portfolio" component={ Portfolio } />
                    <Route path="/login" component={ SignInPage } />
                </Switch>
            </main>
        );
    }
}

export default Main;