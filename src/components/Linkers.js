import React from 'react';


class Linkers extends React.Component {
    render() {
        return(
            <section className="section3">
                <div className="container">
                    <div className="row justify-content-center">
                        <div className="card col-lg-3 col-sm-6 col-md-5 col-12 col-lg-offset-1 margin-linkers">
                            <img className="card-img-top" src={require('../MEDIA/p1.jpg')} alt="Pizza" />
                            <div className="card-body">
                                <p className="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                            </div>
                        </div>
                        <div className="card col-lg-3 col-sm-6 col-xs-6 col-md-5 col-12 col-lg-offset-1 margin-linkers">
                            <img className="card-img-top" src={require('../MEDIA/p2.jpg')} alt="Pizza" />
                            <div className="card-body">
                                <p className="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                            </div>
                        </div>
                        <div className="card col-lg-3 col-sm-6 col-md-5 col-12 col-lg-offset-1 margin-linkers">
                            <img className="card-img-top" src={require('../MEDIA/p3.jpg')} alt="Pizza" />
                                <div className="card-body">
                                    <p className="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                                </div>
                        </div>
                    </div>
                </div>                            
            </section> 
        );
    }
}

export default Linkers;