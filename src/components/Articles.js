import React from 'react';
import { ArticleItem } from './ArticleItem';


class Articles extends React.Component {
    articleToArticleItem = ({ id, title, content }) => {
        return <ArticleItem key={ id } id={ id } title={ title } content={ content } />;
    };
    render() {
        return(
                <div>
                    { this.props.articles.map(this.articleToArticleItem) }
                </div>
        );
    }
}

export default Articles;