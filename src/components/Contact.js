import React from 'react';
import { withFormik, Form, Field } from 'formik';
import * as Yup from "yup";


        
const Contact = (props) => {
    const {
        values,
        errors,
        touched,
        handleBlur,
        handleChange
    } = props;

    return(
        <section id="contact">
            <div className="container">
                <div className="row justify-content-center">
                    <div className="col-md-10 col-xl-8">
                        <Form className="form-margin-contact">
                            <h1 className="text-center">Contact me</h1><br/>
                            <p className="text-center">+00 000 000 00 | ppolichnowski@gmail.com</p><br/>
                            <div>
                                <Field className="form-margin form-control rounded-0" type="text" name="fullname" placeholder="Name"/>
                                { touched.fullname && errors.fullname && <p className="errors">{ errors.fullname }</p> }
                            </div>
                            <div>
                                <Field className="form-margin form-control rounded-0" type="email" name="email" placeholder="Email" />
                                { touched.email && errors.email && <p className="errors">{ errors.email }</p> }
                            </div>
                            <div>
                                <textarea rows="10" cols="50" name="message" className="form-margin form-control rounded-0" type="text" placeholder="Message" value={ values.message } onChange={ handleChange } onBlur={ handleBlur }>
                                </textarea>
                                { touched.message && errors.message && <p className="errors">{ errors.message }</p> }
                            </div>    
                            <button className="btn btn-secondary rounded-0 btn-lg btn-block">Send</button>
                        </Form>
                    </div>
                </div>  
            </div>          
        </section>
    );
}

export default withFormik({
    mapPropsToValues: (props) => ({
        fullname: props.mail.fullname,
        email: props.mail.email,
        message: props.mail.message,
    }),

    validationSchema: Yup.object().shape({
        fullname: Yup.string().min(6, "Fullname must be at least 6 characters").required("Name is a required field"), 
        email: Yup.string().email("Please enter a valid email adress").required("Email is a required field"),
        message: Yup.string().min(6,"Message must contain at least 6 characters").required()
    }),

    handleSubmit(values) {
        alert('Message content \nName: ' + values.fullname + '\nEmail: ' + values.email + '\nMessage: ' + values.message);
    }

})(Contact);