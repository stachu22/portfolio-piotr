import React from 'react';


export function ArticleItem({ id, title, content }) {
    const articleUrl = `http://localhost:3000/Blog/${ id }`;
    return(
        <div className="card1 col-md-6 col-sm-6 col-xs-12 col-12 col-lg-4 article-margin linkers-width">
                <img className="card-img-top" src={require('../MEDIA/p1.jpg')} alt="Pizza" />
                <div className="card-body">
            <h4>{ title }</h4>
            <p>{ content }</p>
            
        <a href={ articleUrl }>Czytaj dalej</a>
                </div>
            </div>
    );
};