export const articlesFetched = (articles) => ({
    type: 'FETCH_ARTICLES_SUCCESS',
    articles
})

export const searchArticles = (text) => ({
    type: 'SEARCH_ARTICLES',
    text
})