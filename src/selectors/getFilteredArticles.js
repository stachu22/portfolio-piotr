export const getFilteredArticles = (articles, text) => {
    const articlesSearch = text.toLowerCase();

    return  articles.filter(article => {
        const title = article.title;
        const content = article.content;

        return (
            title.toLowerCase().includes(articlesSearch) ||
            content.toLowerCase().includes(articlesSearch)
        );
    });
};