export const articles = (state = [], action) => {
    switch (action.type) {
        case 'FETCH_ARTICLES_SUCCESS':
            return [
              ...action.articles
            ]
        default:
            return state
    };
};