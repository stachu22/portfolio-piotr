import { combineReducers } from 'redux';
import { articles } from './articles';
import { articlesSearch } from './articlesSearch';


export default combineReducers({
    articles,
    articlesSearch
})